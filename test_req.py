import json
import requests
import pytest


import api_itin_json as aij
import testparams as tp

def test_html_req_status():
    n = 0
    stat_us = {"req['StatusCode']","req['Status']['Code']"}
    params = tp.params_test_e
    serv = aij.services_3
    for elt in serv:
        url_api = "https://api.ppp38v2.cityway.fr/"
        url = f"{url_api}{elt}"
        r = requests.get(url=url, params=params)
        req = r.json()
        n += 1
        print(f"{elt}")
        print(f"{n} | statut req_html: {r.status_code} | statut req_json: {req['StatusCode']}")
        #if req['Status']['Code'] != "OK" :
        #    print(f"{n} = {req['Status']['comment']}")
        if req['StatusCode'] != 200:
            print(f"{n} = {req['Message']}") 
    assert r.status_code == 308
    