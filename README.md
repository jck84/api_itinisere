# API_itinisere

Documentation, décrivant l’analyse:
- les outils utilisés pour effectuer l’audit
- le déroulement des tests effectués, le constat, l’analyse et la liste des anomalies détaillées
- les suggestions de correction
- des suggestions d’évolution