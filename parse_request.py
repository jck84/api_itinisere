import api_itin_json as aij
import testparams as tp
import requests
import json


# mes fonctions
def status_verif(services,params):
    ''' verif le code status de la requête internet mais pas de la requête json 
    ne prends en 1er paramtre qu'une liste avec un ou plusieurs str '''
    elem = []
    for elt in services:
        api = elt
        url_api = "https://api.ppp38v2.cityway.fr/"
        url = f"{url_api}{api}"
        r = requests.get(url=url, params=params)
        elem.append(r.status_code)
    return elem

def verif_connect_api_service(api,params):
    url_api = "https://api.ppp38v2.cityway.fr/"
    url = f"{url_api}{api}"
    r = requests.get(url=url, params=params)
    data = r.json()
    dta = (json.dumps(data, indent=4, sort_keys=True))
    return data

def verif_connect_api_services(services,params):
    data = []
    url_api = "https://api.ppp38v2.cityway.fr"
    for elt in services:
        url = f"{url_api}{elt}"
        print(url)
        r = requests.get(url=url, params=params)
        tsil = r.json()
        print(tsil['Status']['Code'])
        #data.append(tsil)
    return data

api = "/api/journeyplanner/v2/PlanTripWithBike/json"
apil = ["/api/journeyplanner/v2/PlanTripWithBike/json"]
url_api = "https://api.ppp38v2.cityway.fr"
url = f"{url_api}{api}"

serv = aij.services_1
params_test = tp.params_test_e

# coordonnées géographiques : news = news['trips']['Trip'][0]['Departure']['Site']['Position']
new_status = status_verif(serv,params_test)
print(new_status)
new = verif_connect_api_services(serv,params_test)
#print(new['StatusCode'])
#print(new['Status']['Code'])
print(new)
