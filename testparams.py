


# mes params_test commun
params_test_e ={}
params_test_k = {"Key": "8e8e4bfde8fb1f272ae8df6888010060"} # ici votre clé 


# === journeyplanner/v2 === toufik
    #requis journeyplanner/v2

req_rs1 = ["Date","Algorithm"]

# mes params_test
params_rs1_D = {"Date": "2020-03-23"}
params_rs1_DA = {"Date": "2020-03-23", "Algoritm":"FASTEST"}

#les paramètres requis
params_req1_KD = {"Key": "8e8e4bfde8fb1f272ae8df6888010060","Date": "2020-03-23"}
params_req1_KDA = {"Key": "8e8e4bfde8fb1f272ae8df6888010060","Date": "2020-03-23", "Algoritm":"SHORTEST"}

#les paramètres réels
params_reel1_KDP = {"Key": "8e8e4bfde8fb1f272ae8df6888010060","Date": "2020-03-23","DepLat": 45.1667,"DepLon": 5.7167,"ArrLat": 45.1397972107,"ArrLon": 5.71923685074}
params_reel1_DP = {"Date": "2020-03-23","DepLat": 45.1667,"DepLon": 5.7167,"ArrLat": 45.1397972107,"ArrLon": 5.71923685074}


# === map/v2 === toufik
    #requis map/v2
    
req_rs2 = ["StopId","Line","Direction","LatitudeMin","LatitudeMax",
            "LongitudeMin","LongitudeMax","Latitude","Longitude",
            "Distance","Road","Number","RoadIds","Street"
    ]