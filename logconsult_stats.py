import collections

# Compter le nombre d'erreurs et de critiques dans le log de collecte des erreurs
def Some_Stats():
    stat = open("collect_error.txt", 'r')
    error = stat.read().count("[ERROR]")
    crit = stat.read().count("[CRITICAL]")
    total = (error + crit)

    print(f"Il y a {total} erreurs au total")
    print(f"Le nombre d'ERROR est de : {error}")
    print(f"Le nombre de CRITICAL est de : {crit}")


# Compter le nombre d'erreurs relatives à une méthode v2 ou v3
def Some_Errors():
    stat = open("collect_error.txt", 'r')
    version2 = 0
    version3 = 0
    for line in stat:
        if "v2" in line:
            version2 += 1
        elif "v3" in line:
            version3 += 1
    print(f"Nombre de v2 errors :", version2)
    print(f"Nombre de v3 errors :", version3)
    

# Afficher les méthodes causant des erreurs et leur nombre     
def Compter_Methodes():
    stat = open("collect_error.txt", 'r')
    my_list = []    
    for line in stat:
        champs = line.split(" - ")
        methode = champs[1]
        requete = methode.split(" – ")
        version = requete[1]
        my_list.append(version)
        split = version.split("\n")
    ## Afficher la liste des méthodes récupérées
    #print(my_list)
    counter = collections.Counter(my_list)
    print("Les erreurs sont réparties de la façon suivante :","\n",counter)
    ## Afficher les 3 API contenant le plus d'erreurs
    #print(counter.most_common(3))   

   
Some_Stats()
Some_Errors()
Compter_Methodes()