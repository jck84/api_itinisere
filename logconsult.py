import os

logfiles = ['2020-03-22TSVC.log', '2020-03-23TSVC.log', '2020-03-24TSVC.log', '2020-03-25TSVC.log']

# Collecter les erreurs dans un fichier dédié
def Get_Error():
    # boucler sur chaque log
    for elem in logfiles:
        # ouvrir les logs et créer un fichier de collecte des erreurs
        with open(f"{elem}", "r", encoding="windows-1253") as log_file, open("collect_error.txt", "a") as errorlog:
            # lire le log ligne par ligne
            for line in log_file.readlines():
                # écriture des lignes ERROR et CRITICAL dans le fichier de collecte
                if "[ERROR]" in line or "[CRITICAL]" in line:
                    errorlog.write(line)
        

def SupprimerDoubles():
    ls = []

    # ouvrir le fichier en lecture seule
    with open("collect_error.txt", 'r') as test_file:
        # lire le fichier ligne par ligne
        for line in test_file:
            # copier la ligne dans la liste si elle n'y est pas déjà
            if line not in ls:
                ls.append(line)

    # réouvrir le fichier mais en mode écriture (ce qui effacera le contenu existant) et écrire les lignes de la liste
    with open("collect_error.txt", 'w') as test_file:
        for line in ls:
            test_file.write(line)


Get_Error()
SupprimerDoubles()