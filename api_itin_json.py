base = "https://api.ppp38v2.cityway.fr/"

# === journeyplanner/v2 === toufik
    #requis journeyplanner/v2
req_rs1 = ["Date","Algorithm"
    ]

services_1 = [
    "/api/journeyplanner/v2/BikeTrip/json", # params_reel1_DP
    "/api/journeyplanner/v2/CarTrip/json", # params_reel1_DP
    "/api/journeyplanner/v2/CarSharing/json", 
    "/api/journeyplanner/v2/PlanTrip/json",  
    "/api/journeyplanner/v2/WalkTrip/json", 
    "/api/journeyplanner/v2/PlanTripWithBike/json", 
    "/api/journeyplanner/v2/PlanTripWithCar/json",   
    "/api/journeyplanner/v2/DetailedTrip/json",
    "/api/journeyplanner/v2/GetTripPoints/json",    
    "/api/journeyplanner/v2/PlanTripWithBikeSharing/json",    
    "/api/journeyplanner/v2/CarTrip/Arrival/Coordinates/json",
    "/api/journeyplanner/v2/CarTrip/Departure/Coordinates/json"
    ]

# === map/v2 === toufik
    #requis map/v2
req_rs2 = ["StopId","Line","Direction","LatitudeMin","LatitudeMax",
            "LongitudeMin","LongitudeMax","Latitude","Longitude",
            "Distance","Road","Number","RoadIds","Street"
    ]

services_2 = [
    "/api/map/v2/GetItinerariesOfStop/json",
    "/api/map/v2/GetItinerary/json",
    "/api/map/v2/GetLineStops/json",
    "/api/map/v2/GetLineStopsByBoundingBox/json",
    "/api/map/v2/GetLocalitiesZones/json",
    "/api/map/v2/GetNearestLineStops/json",
    "/api/map/v2/GetNearestPlace/json",
    "/api/map/v2/GetNearestRoad/json",
    "/api/map/v2/GetPlacesByBoundingBox/json",
    "/api/map/v2/GetRoadAndNearestSection/json",
    "/api/map/v2/GetRoadSection/json",
    "/api/map/v2/GetRoadSections/json",
    "/api/map/v2/GetStreetSection/json"  
]

# === v3/stop  === adrien
    #requis journeyplanner/v2
req_rs3 = ["Date","Algorithm"
            ]

services_3 = [
    "/api/transport/v3/stop/GetAirports/json",
    "/api/transport/v3/stop/GetLineStops/json",
    "/api/transport/v3/stop/GetLineStopsOrder/json",
    "/api/transport/v3/stop/GetLogicalStops/json",
    "/api/transport/v3/stop/GetOppositeStop/json",
    "/api/transport/v3/stop/GetStop/json",
    "/api/transport/v3/stop/GetStopByLine/json",
    "/api/transport/v3/stop/GetStopCluster/json",
    "/api/transport/v3/stop/GetStops/json",
    "/api/transport/v3/stop/GetStopsAndClusters/json",
    "/api/transport/v3/stop/GetStopsByBoundingBox/json",
    "/api/transport/v3/stop/GetStopsByCode/json",
    "/api/transport/v3/stop/GetStopsByImportIds/json",
    "/api/transport/v3/stop/GetStopsByLine/json",
    "/api/transport/v3/stop/GetStopsByLineForPartners/json",
    "/api/transport/v3/stop/GetStopsByLines/json",
    "/api/transport/v3/stop/GetStopsDetails/json",
    "/api/transport/v3/stop/GetVehicleJourneyStops/json",
    "/api/transport/v3/stop/SearchStops/json",
    "/api/transport/v3/stop/GetArrLogicalStopsBetweenLocalities/json",
    "/api/transport/v3/stop/GetDepLogicalStopsBetweenLocalities/json",
    "/api/transport/v3/stop/GetStopByQrCode/json" 
]

# === v3/timetable  === adrien
    #requis journeyplanner/v2
req_rs4 = ["Date","Algorithm"
    ]

services_4 = [
    "/api/transport/v3/timetable/GetCirculationDatesOfLines/json",
    "/api/transport/v3/timetable/GetLineDays/json",
    "/api/transport/v3/timetable/GetLineHours/json",
    "/api/transport/v3/timetable/GetLineRealTimeState/json",
    "/api/transport/v3/timetable/GetMonitoredStopPoints/json",
    "/api/transport/v3/timetable/GetMonitoredVehicleJourney/json",
    "/api/transport/v3/timetable/GetNetworkRealTimeState/json",
    "/api/transport/v3/timetable/GetNetworkRealTimeStateByLogicalStop/json",
    "/api/transport/v3/timetable/GetNextDeparturesAndArrivals/json",
    "/api/transport/v3/timetable/GetNextStopHours/json",
    "/api/transport/v3/timetable/GetStopHours/json",
    "/api/transport/v3/timetable/GetVehicleJourneyDates/json"
]

# === traffic/v2  === sofien
    #requis traffic/v2
req_rs5 = ["Date","Algorithm"
    ]

services_5 = [
    "/api/traffic/v2/GetClosureList/json",
    "/api/traffic/v2/GetEvents/json",
    "/api/traffic/v2/GetPmvs/json",
    "/api/traffic/v2/GetRoadLinkDetails/json",
    "/api/traffic/v2/GetRoadLinkDetailsV2/json",
    "/api/traffic/v2/GetSituationRecords/json",
    "/api/traffic/v2/GetSituationRecordsInPerimeter/json",
    "/api/traffic/v2/GetTrafficStatus/json",
    "/api/traffic/v2/GetTravelTimes/json",
    "/api/traffic/v2/GetWinterDrivings/json"
]

# === v3/trippoint  === sofien
    #requis journeyplanner/v2
req_rs6 = ["Date","Algorithm"
    ]

services_6 = [
    "/api/transport/v3/trippoint/GetCategoriesIds/json",
    "/api/transport/v3/trippoint/GetLocalities/json",
    "/api/transport/v3/trippoint/GetLocalitiesByPointTypeAndCategory/json",
    "/api/transport/v3/trippoint/GetLocality/json",
    "/api/transport/v3/trippoint/GetPlaceAddresses/json",
    "/api/transport/v3/trippoint/GetPlaceByLocality/json",
    "/api/transport/v3/trippoint/GetPlaceCategories/json",
    "/api/transport/v3/trippoint/GetPlaceInformations/json",
    "/api/transport/v3/trippoint/GetTripPoint/json",
    "/api/transport/v3/trippoint/GetTripPointLetterIndex/json",
    "/api/transport/v3/trippoint/GetTripPointLetterIndexV2/json",
    "/api/transport/v3/trippoint/GetTripPoints/json",
    "/api/transport/v3/trippoint/GetTripPointsDetails/json",
    "/api/transport/v3/trippoint/GetTripPointsLetter/json",
    "/api/transport/v3/trippoint/GetTripPointsLetterV2/json"
]

# === v3/disruption   === jean-charles
    #requis journeyplanner/v2
req_rs7 = ["Date","Algorithm"
    ]

services_7 = [
    "/api/transport/v3/disruption/GetActiveDisruptions/json",
    "/api/transport/v3/disruption/GetDisruptedLineGroups/json",
    "/api/transport/v3/disruption/GetDisruptedLines/json",
    "/api/transport/v3/disruption/GetDisruptedLinesStops/json",
    "/api/transport/v3/disruption/GetDisruptionsDetails/json",
    "/api/transport/v3/disruption/GetDisruptionsInBoundingBox/json",
    "/api/transport/v3/disruption/GetDisruptionTypes/json",
    "/api/transport/v3/disruption/GetExtendedActiveDisruptions/json",
    "/api/transport/v3/disruption/GetLinesStatus/json",
    "/api/transport/v3/disruption/GetTerritoriesStatus/json",
    "/api/transport/v3/disruption/GetVehicleJourniesStatus/json"
]

# === v3/line  === jean-charles
    #requis journeyplanner/v2
req_rs8 = ["Date","Algorithm"
    ]

services_8 = [
    "/api/transport/v3/line/GetLines/json",
    "/api/transport/v3/line/GetLinesAttributes/json",
    "/api/transport/v3/line/GetLinesDetails/json",
    "/api/transport/v3/line/GetLinesInPerimeter/json",
    "/api/transport/v3/line/GetLinesShapes/json",
    "/api/transport/v3/line/GetNextPassingTime/json",
    "/api/transport/v3/line/GetStructuringLines/json"
]