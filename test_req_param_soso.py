import pytest
import requests
import json


url = "https://api.ppp38v2.cityway.fr/api/transport/v3/trippoint/json"

url1 = "https://api.ppp38v2.cityway.fr"

services_6 = [
    "/api/transport/v3/trippoint/GetCategoriesIds/json",
    "/api/transport/v3/trippoint/GetLocalities/json",
    "/api/transport/v3/trippoint/GetLocalitiesByPointTypeAndCategory/json",
    "/api/transport/v3/trippoint/GetLocality/json",
    "/api/transport/v3/trippoint/GetPlaceAddresses/json",
    "/api/transport/v3/trippoint/GetPlaceByLocality/json",
    "/api/transport/v3/trippoint/GetPlaceCategories/json",
    "/api/transport/v3/trippoint/GetPlaceInformations/json",
    "/api/transport/v3/trippoint/GetTripPoint/json",
    "/api/transport/v3/trippoint/GetTripPointLetterIndex/json",
    "/api/transport/v3/trippoint/GetTripPointLetterIndexV2/json",
    "/api/transport/v3/trippoint/GetTripPoints/json",
    "/api/transport/v3/trippoint/GetTripPointsDetails/json",
    "/api/transport/v3/trippoint/GetTripPointsLetter/json",
    "/api/transport/v3/trippoint/GetTripPointsLetterV2/json"
]

services_5 = [
    "/api/traffic/v2/GetClosureList/json",
    "/api/traffic/v2/GetEvents/json",
    "/api/traffic/v2/GetPmvs/json",
    "/api/traffic/v2/GetRoadLinkDetails/json",
    "/api/traffic/v2/GetRoadLinkDetailsV2/json",
    "/api/traffic/v2/GetSituationRecords/json",
    "/api/traffic/v2/GetSituationRecordsInPerimeter/json",
    "/api/traffic/v2/GetTrafficStatus/json",
    "/api/traffic/v2/GetTravelTimes/json",
    "/api/traffic/v2/GetWinterDrivings/json"
]

payload ={
   "Key": "0474462edcd495696f081c14c0181d77",
    #"Date": "2020-03-26",
    #"DepLat": 45.1667,
    #"DepLon": 5.7167,
    #"ArrLat": 45.1397972107,
    #"ArrLon": 5.71923685074,
    #"PostalCode": 38000,
    "LocalityIds": 1,
    #"TripPoint": 38,
    #"PlaceIds": " 6 rue lindbergh"
    "PointType": "Poi"
}

@pytest.mark.parametrize("urls",services_6)
#@pytest.mark.parametrize("urls1",services_5)
def test_url_service6(urls):
    response = requests.get(url=url1+urls, params=payload)
    assert response.status_code == 200
    #assert response.body['url'] == url
    #assert response.headers['Content-Type'] == "application/json"
    #assert response.request.method == 'HEAD'
    #assert response.request.method == 'GET'
    print(response)
    print(response.content)


def test_type():
    if test_url_service6 == 200:
        print("statue 200")
    elif test_url_service6 == 400:
        print("statue 400")
    elif test_url_service6 == 300:
        print("statue 300")
    elif test_url_service6 == 500:
        print("statue 500")

def test_types():
    if test_url_service6 == dict:
        print("dico")
    elif test_url_service6 == list:
        print("list")
    elif test_url_service6 == tuple:
        print("tuple")

################################################################################################

@pytest.mark.parametrize("apis",services_5)
def test_essai_url_service5(apis):
    responses = requests.get(url=url1+apis, params=payload)
    assert responses.status_code == 200
    print(responses)
    